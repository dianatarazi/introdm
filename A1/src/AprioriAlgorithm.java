import java.util.*;
import java.io.*;


public class AprioriAlgorithm {
	
	public HashSet<String> database;
	private HashSet<HashSet<String>> itemsets;
	private String dataFile;
	private int numItems;
	private int numTransactions;
	private HashMap<String, HashSet<String>> transactions;
	public HashSet<HashSet<String>> frequentItemsets;
	private HashMap<HashSet<String>, Double> itemSupport;
	private double minSupport;
	private HashMap<HashSet<String>, Integer> itemCount;

	public AprioriAlgorithm() {
		//itemsets = new HashSet<>();
		transactions = new HashMap<String, HashSet<String>>();
		database = new HashSet<String>();
		frequentItemsets = new HashSet<HashSet<String>>();
		//readFile();
		

	}

	//reads file and computes numItems and numTransactions
	public void readFile() throws Exception {
		//setting dataFile
		dataFile = "../data/mergedfileproducts.csv";
		
		HashSet<String> nextValue;
		HashSet<String> oldValue = new HashSet<String>();
		String nextKey;
		String line = "";
	
		BufferedReader data = new BufferedReader(new FileReader(dataFile));
            data.readLine();
            while ((line = data.readLine()) != null) {
            	if (line.matches("\\s*")) continue; //friendly with empty lines
            	numItems++; 
            	String[] trans = line.split(",");
            	nextValue = new HashSet<String>();
            	nextKey = trans[0];
            	nextValue.add(trans[2]);
            	database.add(trans[2]); //makes database of all possible items in file
            	if (transactions.containsKey(nextKey)) {
            		//transactions.put(nextKey,transactions.get(nextKey).addAll(nextValue));
            		oldValue = transactions.get(nextKey);
            		oldValue.addAll(nextValue);
            		transactions.put(nextKey, oldValue);
            	} else {
            		transactions.put(nextKey, nextValue);
            	}
            }
            numTransactions = transactions.size();
            //System.out.println(transactions);
            //return itemsets;
            //System.out.println(database);
           

	}

	/*public void calculateFrequencyItems() throws Exception {
		HashMap<String, Double> itemCount = new HashMap<String, Double>();
		double freq;
		double support;
		for (String s : database) {
			freq = Collections.frequency(database, s);
			support = freq / (double) (database.size());
			itemCount.put(s, support);
		}

	}*/

	private void setMinSup(String[] args) throws Exception {
		if (args.length >=1 ) {
			minSupport = (Double.valueOf(args[0]).doubleValue());
		} else {
			minSupport = .8; //by default
		}
		if (minSupport > 1 || minSupport < 0) throw new Exception("Bad value");
	}

	//generates sets of all possible items
	public void createItemsetsOfSize1() {
		itemsets = new HashSet<HashSet<String>>();
		HashSet<String> cand = new HashSet<String>();
		for (String s : database) {
			cand = new HashSet<String>();
			cand.add(s);
			itemsets.add(cand);
		}

		//System.out.println(itemsets);
	}
	
	//generate pairs from the frequent items of size 1
	private void createItemsetsOfSize2() {
		HashSet<String> newset = new HashSet<String>();
		for (HashSet<String> s: frequentItemsets) {
			for (HashSet<String> t : frequentItemsets) {
				if (s.equals(t) == false) {
					newset = ((HashSet)s.clone());
					newset.addAll(t);
					itemsets.add(newset);
				}
				

			}
		}
		frequentItemsets.addAll(itemsets);
		//System.out.println(frequentItemsets);
	}
	
	//calculates popular items
	private void calculateFrequency()  {
		//if the set is contained in the value of a transaction then increase count
		//it would be compared with the hashset in the transaction map
		itemCount = new HashMap<HashSet<String>, Integer>();
		itemSupport = new HashMap<HashSet<String>, Double>(); //frequent candiadates for current itemset
		
		int count = 1;
		//Iterator<HashSet<String>> items = itemsets.iterator();

		//for each transaction
		for(Map.Entry<String, HashSet<String>> trans : transactions.entrySet()) {
			//for each candidate
			for (HashSet<String> itemset : itemsets) {
				
				if (trans.getValue().containsAll(itemset)) {
					if (itemCount.containsKey(itemset)) {
						int getCount = itemCount.get(itemset);
						//count++;
						itemCount.replace(itemset, getCount + 1);
					} else {
						itemCount.put(itemset, count);
					}
				} 
			} 
		} //System.out.println(itemCount);
		
		//calculate minimmum support for each itemset
		for (Map.Entry<HashSet<String>, Integer> entry : itemCount.entrySet()) {
			double support = entry.getValue() / (double) (numTransactions);
			itemSupport.put(entry.getKey(), support*100);
			if (support >= minSupport) {	
				frequentItemsets.add(entry.getKey());
			}
			
		} //System.out.println(frequentItems);


	}

	
	//calculates Confidence and lift of two items (this code is not complete)
	private void confidenceAndLift(String x, String y) {
		//confidence
		//for itemsets in itemSupport, find the itemset that contains has exactly x and y 
		//get the support of this itemset and divide it by the support of x
		HashSet<String> itemX = new HashSet<String>();
		itemX.add(x);
		HashSet<String> itemY = new HashSet<String>();
		itemY.add(y);
		for (Map.Entry<HashSet<String>, Double> entry : itemSupport.entrySet()) {
			if (entry.getKey().equals(itemX) && entry.getKey().equals(itemY)) {
				//itemSupport.get(itemX);
			}
			
		}
			

		//lift
		//same thing but in the denominator multiply x and y
		//if result is more than 1, it is likely, if less than 1 it is unlikely, 0 no association
	} 


	//runs algorithm
	private void start() throws Exception{
		readFile();
		createItemsetsOfSize1();
		calculateFrequency();
		createItemsetsOfSize2();
		calculateFrequency();

	}


	public static void main(String[] args) throws Exception {
		AprioriAlgorithm apriori = new AprioriAlgorithm();
		apriori.setMinSup(args);
		apriori.start();
		//apriori.confidenceAndLift();
		System.out.println("There are " + apriori.frequentItemsets.size() + " popular items");

		System.out.println("Support of all itemsets: ");
		for (HashSet<String> key : apriori.itemSupport.keySet()) {
    		System.out.println(key + " = " + apriori.itemSupport.get(key));

		}
		


		
	}

}
