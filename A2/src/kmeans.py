# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
from numpy import genfromtxt
#import sklearn.cluster as sklc
#from sklearn.cluster import KMeans
#import pandas as pd
#from collections import Counter

data= genfromtxt('/Users/abby/Downloads/SpaceThings.csv', delimiter=',')

def mandist(xrow,yrow):
#make empty list
    distance= 0
#pull out variables in row
    for i in range(len(xrow)):
        if not len(xrow)==len(yrow):
            break
        itvalue=abs(xrow[i]-yrow[i])
        distance = sum(distance+itvalue)
    return distance
    
class KMeans:
    def __init__(self, k=2, tol=0.001, max_iter=300):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter
        

    def fit(self,data):
        
        self.centroids = {}
    
       
# Creates an object for every centroid
        
        for i in range(self.k):
            self.centroids[i]=data[i]
            
#Creates a dictionary at the start of every iteration,
#and ensures that it will only iterate for the max number of iterations
        
        for i in range(self.max_iter):
            self.classifications = {}
            
#Creates an object for every centroid within each dictionary
            
            for i in range(self.k):
                self.classifications[i] = []
            
#For every observation in the data, it finds the euclidean distance from each centroid
#and finds the minimum distance
#Then stores the observation in a list under the centroid classification within the dictionary of classifications
                
            for featureset in data:
                distances = [mandist(featureset, self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)
                
#Stores iteration data to compare to next iteration
                
            prev_centroids=dict(self.centroids)

            for classification in self.classifications:
                self.centroids[classification] = np.average(self.classifications[classification], axis =0)
            
            optimized = True
            
            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                if np.sum((original_centriod-current_centriod)/original_centriod*100) > self.tol:
                    optimized = False
            if optimized:
                break
            
                
    def cluster_count(self, data):
        for classification in self.classifications:
            print(len(self.classifications[classification]))
            

    
clf= KMeans()

clf.fit(data)

clf.cluster_count(data)

#for centroid in clf.centroids:
    #plt.scatter(clf.centroids[centroid][0], clf.centroids[centroid][1], marker= "o", color="k", s=150,
              #  linewidths=5)
#for classification in clf.classifications[classification]:
    #color = colors[classification]
   # for featureset in clf.classifications[classification]:
        #plt.scatter(featureset[0], featureset[1], marker= "x", color=color, s=150, linewidths=5)

#plt.show()
        

 
 #To compare to the sklearn.clusters KMeans function, we implemented it and printed its return
data= pd.read_csv('/Users/LS/Downloads/Sky.csv')
test= KMeans(n_clusters=3)
test.fit(data)

print(Counter(test.labels_))
 
 