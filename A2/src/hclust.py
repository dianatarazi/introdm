#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 15:13:38 2018

@author: ls
"""
import matplotlib.pyplot as plt 
from matplotlib import style 
style.use('ggplot')
import numpy as np
import pandas as pd 
import sklearn as skl
#import sklearn.cluster as skl
#from sklearn.cluster import cluster
#from numpy import genfromtxt

data= pd.read_csv('/Users/LS/Downloads/Sky.csv', delimiter=',',index_col=0)

# =============================================================================
# X = np.array([[1,2], [1.5,1.8],[5,8],[8,8],[1,0.6],[9,11],[8,2],[10,2],[9,3],])
# 
# colors = 10*["g","r","c","b","k"]
# =============================================================================

class bottom_up:
    def __init__(self, radius=100):
        self.radius = radius
 
    
    def fit(self, data):
        centroids = {}
    
#Setting all the data points as original centroids before iteration.        
        for i in range(len(data)): 
            centroids[i] = data[i]
  
#Making an empty list to put newly discovered more accurate centroids in. 
        while True: 
            new_centroids =[]
            for i in centroids: 
#The ones that are within the radius. 
                in_bandwidth = []
                centroid = centroids[i]
                for featureset in data: 
#checking how far the the new/next row/datapoint/featureset is from the centroid
                    if np.linalg.norm(featureset-centroid) < self.radius:
#if it is close enough as determined by radius to be part of this cluster it will append 
                        in_bandwidth.append(featureset)
#determining a new centroid from the newly added vectors taken from the added featureset
                new_centroid = np.average(in_bandwidth,axis=0)
                new_centroids.append(tuple(new_centroid))

#sorting the new centroids in order to see if they have dublicates            
            uniques = sorted(list(set(new_centroids)))

#coping centroids dict             
            prev_centroids = dict(centroids)

#now a new centroids dictionary            
            centroids = {}
            for i in range(len(uniques)): 
                    centroids[i] = np.array(uniques[i])
                    
            optimized = True

#checking if the new centroids and previous ones are equal to see if clustering is optimized 
            for i in centroids:
                if not np.array_equal(centroids[i], prev_centroids[i]):
                    optimized = False 
                if not optimized :
                    break 
            if optimized :
                break
    
        self.centroids = centroids 
        
    def predict(self, data): 
        pass 
    
clf = bottom_up()
clf.fit(data)

centroids = clf.centroids 

# =============================================================================
# plt.scatter(X[:,0], X[:,1], s=150)
# plt.show()
# 
# for c in centroids:
#     plt.scatter(centroids[c][0], centroids[c][1], color='k', marker='*', s=150)
# =============================================================================
    
print(centroids)

                
            
                        
        